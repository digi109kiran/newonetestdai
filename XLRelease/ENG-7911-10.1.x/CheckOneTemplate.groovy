// Exported from:        http://LAPTOP-95VG3LUV:5516/#/templates/Folder93b9491fdf0d4a769f2df5905760b0b0-Release643479fb216341b4b38dda0746a2fc4e/code
// Release version:      10.0.0-20210203-123974
// Date created:         Mon Dec 06 16:40:59 IST 2021

xlr {
  template('CheckOneTemplate') {
    folder('ENG-7911-10.1.x')
    scheduledStartDate Date.parse("yyyy-MM-dd'T'HH:mm:ssZ", '2021-12-06T09:00:00+0530')
    phases {
      phase('Phase One') {
        color '#0079BC'
        tasks {
          manual('Task One') {
            
          }
          manual('Task Two') {
            
          }
          manual('Task Three') {
            
          }
        }
      }
    }
    
  }
}